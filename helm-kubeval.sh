#!/bin/bash

echo "Running kubeval validations..."

if ! [ -x "$(command -v kubeval)" ]; then
  echo 'Error: kubeval is not installed.' >&2
  exit 1
fi

helm template . | kubeval "$@"

exit $?
