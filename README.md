# K8S pre-commit Hooks

A set of pre-commit hooks for kubernetes projects.

## Available hooks

### helm-kubeval

Validate the Kubernetes YAML generated with Helm.

[kubeval](https://www.kubeval.com/) must be available in your environment.

By default kubeval is executed with "--ignore-missing-schemas" and "--strict"
options and you can override them in the pre-commit config.

Example:

```YAML
  - repo: https://gitlab.com/gpr-oss/k8s-pre-commit-hooks
    rev: v0.1.0
    hooks:
      - id: helm-kubeval
        args: ["--strict", "-v", "1.14.0"]
```
